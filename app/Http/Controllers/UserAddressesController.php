<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\UserAddress;
use App\Http\Requests\UserAddressRequest;

class UserAddressesController extends Controller{
    // 收货地址列表
    public function index(Request $request){

        return view('user_addresses.index',[
            'addresses'=>$request->user()->userAddresses
        ]);
    }

    // 创建收货地址页面
    public function create(){
        return view('user_addresses.create_and_edit',['address' => new UserAddress()]);
    }

    // 创建收货地址逻辑
    public function store(UserAddressRequest $request){
        $request->user()->UserAddresses()->create($request->only([
            'province','city','district','address','zip','contact_name','contact_phone',
        ]));

        return redirect()->route('user_addresses.index');
    }

    // 编辑收货地址
    public function edit(UserAddress $user_address){
        $this->authorize('own',$user_address);

        return view('user_addresses.create_and_edit',['address' => $user_address]);
    }

    // 编辑收货地址逻辑
    public function update(UserAddress $user_address,UserAddressRequest $request){
        $this->authorize('own',$user_address);
        $user_address->update($request->only([
            'province','city','district','address','zip','contact_name','contact_phone',
        ]));

        return redirect()->route('user_addresses.index');
    }

    // 删除收货地址
    public function destroy(UserAddress $user_address){
        $this->authorize('own',$user_address);
        $user_address->delete();
        // return redirect()->route('user_addresses.index');
        return [];
    }
}
