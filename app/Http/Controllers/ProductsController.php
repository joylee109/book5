<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Exceptions\InvalidRequestException;

class ProductsController extends Controller{
    public function index(Request $request){
        // 创建一个查询构造器
        $builder = Product::query()->where('on_sale',1);
        // 判断是否有提交 search 参数，如果有就赋值给 $search 变量
        // search 参数用来模糊搜索商品
        if($search = $request->input('search','')){
            $like = '%'.$search.'%';
            // 模糊搜索商品标题、商品详情、sku 标题、sku 描述
            $builder->where(function($query) use($like){
                $query->where('title','like',$like)
                        ->orWhere('description','like',$like)
                        ->orWhereHas('skus',function($query) use($like){
                            $query->where('title','like',$like)
                                    ->orWhere('description','like',$like);
                        });
            });
        }

        // 是否有提交 order 参数，如果有就赋值给 $order 变量
        // order 参数用来控制商品的排序规则
        if($order = $request->input('order','')){
            // 是否是以_asc 或者 _desc 结尾
            if(preg_match('/^(.+)_(asc|desc)$/',$order,$m)){
                if(in_array($m[1],['price','sold_count','rating'])){
                    // 根据传入的排序值来构造排序函数
                    $builder->orderBy($m[1],$m[2]);
                }
            }
        }
        $products = $builder->paginate(16);
        $filters = [
            'search' => $search,
            'order' => $order
        ];
        return view('products.index',compact('products','filters'));
    }

    public function show(Product $product,Request $request){
        // 判断商品是否已经上架，如果没有上架则抛出异常.
        if(!$product->on_sale){
            throw new InvalidRequestException('商品未上架');
        }
        $favored = false;
        // 用户未登录是返回的是null,已登录是返回的是对应的用户对象
        if($user = $request->user()){
            // 从当前用户已收藏的商品中搜索id 为当前商品id 的商品
            // boolval() 函数用于把值转为布尔值
            $favored = boolval($user->favoriteProducts()->find($product->id));
        }

        return view('products.show',compact('product','favored'));
    }

    // 收藏商品
    public function favor(Product $product,Request $request){
        $user = $request->user();
        if($user->favoriteProducts()->find($product->id)){
            return [];
        }

        $user->favoriteProducts()->attach($product);
        return [];
    }

    // 取消收藏商品
    public function disfavor(Product $product,Request $request){
        $user = $request->user();
        $user->favoriteProducts()->detach($product);
        return [];
    }

    // 我的收藏商品列表
    public function favorites(Request $request){
        $products = $request->user()->favoriteProducts()->paginate(16);
        return view('products.favorites',compact('products'));
    }
}
