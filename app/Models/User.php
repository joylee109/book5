<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','email_verified',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // 收货地址
    public function userAddresses() {
        return $this->hasMany(UserAddress::class);
    }

    // 用户收藏的产品
    public function favoriteProducts(){
        // withTimestamps() 代表中间表带有时间戳字段。
        return $this->belongsToMany(Product::class,'user_favorite_products','user_id','product_id')->withTimestamps()->orderby('user_favorite_products.created_at','desc');
    }

    // 用户的购物车
    public function cartItems(){
        return $this->hasMany(CartItem::class);
    }
}
