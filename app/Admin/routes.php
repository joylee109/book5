<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    # 后台首页
    $router->get('/', 'HomeController@index');
    # 后台用户列表页面
    $router->get('users','UsersController@index');
    # 商品列表页面
    $router->get('products','ProductsController@index');
    # 创建商品
    $router->get('products/create','ProductsController@create');
    # 创建商品逻辑
    $router->post('products','ProductsController@store');
    # 编辑商品
    $router->get('products/{id}/edit','ProductsController@edit');
    # 编辑商品逻辑
    $router->put('products/{id}','ProductsController@update');
});
