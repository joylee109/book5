@extends('layouts.app')
@section('title','操作成功')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">操作成功</div>
        <div class="panel-body text-center">
            <h2>{{ $msg }}</h2>
            <a class="btn btn-primary" href="{{ url('/') }}">返回首页</a>
        </div>
    </div>
@stop