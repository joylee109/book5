<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-COMPATIBLE" content="IE=edge" >
        <meta name="viewport" content="width=device-width,initial-scale=1">
        <meta name="csrf" content="{{ csrf_token() }}">
        <title>@yield('title','Book5 Shop') - Laravel 电商教程</title>
        <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app" class="{{ route_class() }}-page">
            @include('layouts._header')
            <div class="container">
                @yield('content')
            </div>
            @include('layouts._footer')
        </div>
        {{-- js 脚本 --}}
        <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
        @yield('scriptsAfterJs')
    </body>
</html>