<?php

return [

    /*
     * Laravel-admin name. 站点标题
     */
    'name' => 'Book5 shop',

    /*
     * Logo in admin panel header.页面顶部Logo
     */
    'logo' => '<b>Book5</b> Shop',

    /*
     * Mini-logo in admin panel header.页面顶部小Logo
     */
    'logo-mini' => '<b>B-S</b>',

    /*
     * Route configuration.路由配置
     */
    'route' => [
        # 路由前缀
        'prefix' => 'admin',
        # 控制器命名空间前缀
        'namespace' => 'App\\Admin\\Controllers',
        # 默认中间件列表
        'middleware' => ['web', 'admin'],
    ],

    /*
     * Laravel-admin install directory. Laravel-Admin 的安装目录
     */
    'directory' => app_path('Admin'),

    /*
     * Laravel-admin html title.Laravel-Admin 页面标题
     */
    'title' => 'Book5 Shop 管理后台',

    /*
     * Use `https`. 是否使用https
     */
    'secure' => false,

    /*
     * Laravel-admin auth setting. Laravel-Admin 用户认证设置
     */
    'auth' => [
        'guards' => [
            'admin' => [
                'driver'   => 'session',
                'provider' => 'admin',
            ],
        ],

        'providers' => [
            'admin' => [
                'driver' => 'eloquent',
                'model'  => Encore\Admin\Auth\Database\Administrator::class,
            ],
        ],
    ],

    /*
     * Laravel-admin upload setting.Laravel-Admin 文件上传设置
     */
    'upload' => [
        // 对应filesystem.php 中的disks
        'disk' => 'public',

        'directory' => [
            'image' => 'images',
            'file'  => 'files',
        ],
    ],

    /*
     * Laravel-admin database setting. Laravel-Admin 数据库设置
     */
    'database' => [

        // Database connection for following tables. 数据库连接名称，留空即可
        'connection' => '',

        // User tables and model. 管理员用户表及模型
        'users_table' => 'admin_users',
        'users_model' => Encore\Admin\Auth\Database\Administrator::class,

        // Role table and model.角色表及模型
        'roles_table' => 'admin_roles',
        'roles_model' => Encore\Admin\Auth\Database\Role::class,

        // Permission table and model. 权限表及模型
        'permissions_table' => 'admin_permissions',
        'permissions_model' => Encore\Admin\Auth\Database\Permission::class,

        // Menu table and model. 菜单表及模型
        'menu_table' => 'admin_menu',
        'menu_model' => Encore\Admin\Auth\Database\Menu::class,

        // Pivot table for table above.多对多关联中间表
        'operation_log_table'    => 'admin_operation_log',
        'user_permissions_table' => 'admin_user_permissions',
        'role_users_table'       => 'admin_role_users',
        'role_permissions_table' => 'admin_role_permissions',
        'role_menu_table'        => 'admin_role_menu',
    ],

    /*
     * By setting this option to open or close operation log in laravel-admin.
     * Laravel-Admin 操作日志设置
     */
    'operation_log' => [

        'enable' => true,

        /*
         * Routes that will not log to database.
         *
         * All method to path like: admin/auth/logs
         * or specific method to path like: get:admin/auth/logs
         * 不记操作日志的路由
         */
        'except' => [
            'admin/auth/logs*',
        ],
    ],

    /*
     * @see https://adminlte.io/docs/2.4/layout
     * 页面风格
     */
    'skin' => 'skin-blue-light',

    /*
    |---------------------------------------------------------|
    |LAYOUT OPTIONS | fixed                                   |
    |               | layout-boxed                            |
    |               | layout-top-nav                          |
    |               | sidebar-collapse                        |
    |               | sidebar-mini                            |
    |---------------------------------------------------------|
     */
    'layout' => ['sidebar-mini', 'sidebar-collapse'],

    /*
     * Background image in login page
     */
    'login_background_image' => '',

    /*
     * Version displayed in footer.页面底部展示的版本。
     */
    'version' => '1.5.x-dev',

    /*
     * Settings for extensions.扩展设置
     */
    'extensions' => [

    ],
];
