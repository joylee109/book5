<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/','PagesController@root')->name('root');
Auth::routes();
Route::redirect('/','/products')->name('root');
Route::get('products','ProductsController@index')->name('products.index');


Route::group(['middleware'=> ['auth']],function(){
    // 验证邮箱页面 发送邮件
    Route::get('/email_verify_notice','PagesController@emailVerifyNotice')->name('email_verify_notice');
    // 点击按钮 发送邮件
    Route::get('/email_verification/send','EmailVerificationController@send')->name('email_verification.send');
    // 邮件中的验证链接
    Route::get('/email_verification/verify','EmailVerificationController@verify')->name('email_verification.verify');

    // 加入了验证邮箱的中间件
    Route::group(['middleware' =>['email_verified']],function(){
        // 收货地址列表页面
        Route::get('/user_address','UserAddressesController@index')->name('user_addresses.index');
        // 新增收货地址页面
        Route::get('/user_address/create','UserAddressesController@create')->name('user_addresses.create');
        // 收货地址逻辑
        Route::post('/user_address','UserAddressesController@store')->name('user_addresses.store');
        // 编辑收货地址
        Route::get('/user_address/{user_address}','UserAddressesController@edit')->name('user_addresses.edit');
        // 编辑收货地址逻辑
        Route::patch('/user_address/{user_address}','UserAddressesController@update')->name('user_addresses.update');
        // 删除收货地址
        Route::delete('/user_addresses/{user_address}','UserAddressesController@destroy')->name('user_addresses.destroy');
        // 收藏商品
        Route::post('products/{product}/favorite','ProductsController@favor')->name('products.favor');
        // 取消收藏商品
        Route::delete('products/{product}/favorite','ProductsController@disfavor')->name('products.disfavor');
        // 我的收藏
        Route::get('products/favorites','ProductsController@favorites')->name('products.favorites');
        // 加入购物车
        Route::post('cart','CartController@add')->name('cart.add');
        // 我的购物车列表
        Route::get('cart','CartController@index')->name('cart.index');
        // 删除购物车选项
        Route::delete('cart/{sku}','CartController@remove')->name('cart.remove');

    });

});
// 商品详情
Route::get('products/{product}','ProductsController@show')->name('products.show');
