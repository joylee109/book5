<?php

use Illuminate\Database\Seeder;
use App\Models\UserAddress;
use \Carbon\Carbon;

class UserAddressTableSeeder extends Seeder{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $addresses = factory(UserAddress::class)->times(3)->make()->each(function($address,$index){
            $address->user_id = 1;
            $address->created_at = Carbon::now()->toDateTimeString();
            $address->updated_at = Carbon::now()->toDateTimeString();
        });

        UserAddress::insert($addresses->toArray());

    }
}
