<?php

use Faker\Generator as Faker;
use App\Models\ProductSku;

$factory->define(ProductSku::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->sentence(8),
        'price' => $faker->randomNumber(4),
        'stock' => $faker->randomNumber(5),
    ];
});
